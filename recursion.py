# Name: Joey Carberry
# Date: Jan 7, 2016
# Project: Recursion Project
import math
tenX = 1
tenValue = -1
total = 0
def sumDigits(num):
    global tenX
    global tenValue
    global total
    while num > tenX:
        tenValue += 1
        tenX *= 10
    trueVal = (math.pow(10, tenValue))
    print('Ten Value: ', tenValue)
    newValue = num / trueVal
    print('New Value: ', int(newValue))
    newNum = num - (int(newValue) * trueVal)
    print('New Num: ', newNum)
    total += newValue
    if newNum <= 0:
        if total < 0:
            print('Inputted integer is neg. Error.')
            total = 0
        print('=============================================')
        print()
        print('Total: ', int(total))
        print()
        print('=============================================')
    else:
        tenValue = -1
        tenX = 1
        sumDigits(newNum)
userInput = int(input('Enter an integer: '))
sumDigits(userInput)