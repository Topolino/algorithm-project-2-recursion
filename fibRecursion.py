# Name: Joey Carberry
# Date: Jan 7, 2016
# Project: Recursion Project - Fibonacci Number Thing

def fibIterative(num):
    valueList = [0, 1]
    for x in range(1, num):
        newValue = valueList[x] + valueList[x-1]
        valueList.append(newValue)
    print(valueList)


def F(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return F(n-1) + F(n-2)


userInput = int(input('Enter a Fibonacci Index: '))
print('1.) Iterative')
print('2.) Recursive')
iterOrRecurInput = int(input('Which would you like to do? '))

recursiveList = []
if iterOrRecurInput == 1:
    fibIterative(userInput)
else:
    x = 0
    while x < userInput:
        f1 = F(x)
        recursiveList.append(f1)
        x += 1
    f2 = F(userInput)
    recursiveList.append(f2)
listLen = len(recursiveList)
for y in range(0, listLen):
    print(recursiveList[y])
